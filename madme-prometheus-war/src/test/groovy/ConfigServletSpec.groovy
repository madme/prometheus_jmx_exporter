import groovyx.net.http.HttpResponseException
import groovyx.net.http.RESTClient
import spock.lang.Specification

/**
 * Test for configuration servlet
 */
class ConfigServletSpec extends Specification {

	static portNum = System.getProperty("metrics.port", "18121")

	static client
	static {
		client = new RESTClient("http://localhost:${portNum}/prometheus/")
	}

	def "doPost not supported"() {
		when:
			client.post([path: 'config'])

		then:
			def err = thrown(HttpResponseException)
			err.response.status == 501

	}

	def "doGet returns config"() {
		when:
			def response = client.get([path: 'config'])
		then:
			response.status == 200
			response.contentType == "text/yaml"
			response.headers['x-file-location'] != null
			response.headers['last-modified'] != null
			response.data.text == getClass().getResourceAsStream("/prometeus-it.yaml").text
	}

}
