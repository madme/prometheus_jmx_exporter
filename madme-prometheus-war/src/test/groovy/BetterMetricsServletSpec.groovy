import groovyx.net.http.RESTClient
import spock.lang.Specification

/**
 * Spec for metrics servlet
 */
class BetterMetricsServletSpec extends Specification {
	static portNum = System.getProperty("metrics.port", "18121")

	static client
	static {
		client = new RESTClient("http://localhost:${portNum}/prometheus/")
	}

	def "Metrics have jmx_scrape_start_time_ms"() {
		when:
			def response = client.get([path: ''])
		then:
			response != null
			response.contentType == "text/plain"
			response.data.text.contains("jmx_scrape_start_time_ms")
	}

}
