package com.madme.prometheus.servlets;

import org.apache.commons.io.IOUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Date;

/**
 * Configuration servlet that initializes metrics exporter and allows to trigger reconfiguration.
 * It's a pity that it has to be so old code!
 *
 * It is triumvirate of things -
 * <ol>
 *     <li>BetterMetricsServlet - exposing endpoint</li>
 *     <li>ConfigServlet - providing capability to remotely read / write .yaml config file</li>
 *     <li>ServletContextListener - entity which ensures that updates to configuration are propagated to CollectorRegistry</li>
 * </ol>
 */
public class ConfigServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        ServletContextListener.FileInfo info = ServletContextListener.latestStatus();
        try (PrintWriter pw = resp.getWriter()) {
            if (info.getException() != null) {
                resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                resp.setHeader("Content-Type", "text/plain");
                pw.println(String.format("Failed reading config from file %1$s, at %2$s", info.getFileName(), new Date(info.getLastChangeTs())));
                info.getException().printStackTrace(pw);

            } else {
                resp.setStatus(HttpServletResponse.SC_OK);
                resp.setCharacterEncoding("UTF-8");
                resp.setHeader("Content-Type", "text/yaml");
                resp.setHeader("x-file-location", info.getFileName());
                resp.setDateHeader("last-modified", info.getLastChangeTs());

                try (InputStream is = new FileInputStream(info.fileName)) {
                    IOUtils.copyLarge(new InputStreamReader(is, "UTF-8"), pw);
                }
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.sendError(HttpServletResponse.SC_NOT_IMPLEMENTED, "Posting config not implemented yet.");
    }
}
