package com.madme.prometheus.servlets;

import io.prometheus.client.exporter.common.TextFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;

/**
 *
 */
public class BetterMetricsServlet extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setStatus(HttpServletResponse.SC_OK);
        resp.setContentType(TextFormat.CONTENT_TYPE_004);

        //it is debatable where we need to rate-limit collection - either in registry or in specific collector ?
        try (Writer writer = resp.getWriter()) {
            TextFormat.write004(writer, ServletContextListener.getRegistry().metricFamilySamples());
        }
    }
}