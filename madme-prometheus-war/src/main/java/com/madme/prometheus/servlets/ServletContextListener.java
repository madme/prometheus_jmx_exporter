package com.madme.prometheus.servlets;

import io.prometheus.client.CollectorRegistry;
import io.prometheus.client.hotspot.DefaultExports;
import io.prometheus.jmx.JmxCollector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContextEvent;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

/**
 *
 */
public class ServletContextListener implements javax.servlet.ServletContextListener {


    private final static Timer watchTimer = new Timer("Prometheus config watch", true);
    private final static AtomicReference<FileInfo> lastChange = new AtomicReference<>();

    private static Logger LOGGER = LoggerFactory.getLogger(ServletContextListener.class);

    /**
     * Defines web app entry name that provides config file location
     */
    public final static String CONTEXT_CONFIG_LOCATION_ATTRIBUTE = "prometheus-conf";
    /**
     * Normally that'll be templated and resolved by tomcat, if not defaults to the value below
     */
    public final static String DEFAULT_CONFIG_FILE_LOCATION = "/opt/madme/conf/prometheus/jmx-web-app.yaml";

    /**
     * How ofter we'll check for config file's changes
     */
    private final static int WATCH_HOW_OFTEN = 5;


    public static class FileInfo {
        final long lastChangeTs;
        final String fileName;
        final Throwable exception;

        FileInfo(long lastChangeTs, String fileName) {
            this(lastChangeTs, fileName, null);
        }

        public FileInfo(long lastChangeTs, Throwable exception) {
            this(lastChangeTs, null, exception);
        }

        public FileInfo(long lastChangeTs, String fileName, Throwable exception) {
            this.lastChangeTs = lastChangeTs;
            this.fileName = fileName;
            this.exception = exception;
        }

        public long getLastChangeTs() {
            return lastChangeTs;
        }

        public String getFileName() {
            return fileName;
        }

        public Throwable getException() {
            return exception;
        }
    }

    private static class JmxConfigurer extends TimerTask {
        final CountDownLatch startLatch;
        final File fileToWatch;
        AtomicReference<JmxCollector> collectorRef = new AtomicReference<>();

        JmxConfigurer(CountDownLatch startLatch, File fileToWatch) {
            this.startLatch = startLatch;
            this.fileToWatch = fileToWatch;
        }

        @Override
        public void run() {
            FileReader reader = null;
            FileInfo info = lastChange.get();
            long lastModified = fileToWatch.lastModified();
            if (info != null && info.lastChangeTs == lastModified) {
                return;
            }
            try {
                LOGGER.info("Configuring configure jmx scraper from file {}", fileToWatch);

                //NB: logic below can result in duplicated metric blip for short time
                reader = new FileReader(fileToWatch);
                JmxCollector collector = new JmxCollector(reader);
                JmxCollector old = collectorRef.get();
                if (old != null) {
                    CollectorRegistry.defaultRegistry.unregister(old);
                    old.stop();
                }
                collector.register();
                collectorRef.set(collector);

                //NB: here we speculate that lastModified used for loading collector is same as file API returned previously
                info = new FileInfo(lastModified, fileToWatch.getCanonicalPath());
            } catch (Throwable e) {
                info = new FileInfo(new Date().getTime(), e);
            } finally {
                lastChange.set(info);
                startLatch.countDown();
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    //ignore
                }
            }
        }
    }

    private static volatile boolean isInitialised = false;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        final CountDownLatch jmxDone = new CountDownLatch(1);
        String fileName = sce.getServletContext().getInitParameter(CONTEXT_CONFIG_LOCATION_ATTRIBUTE);

        if (fileName == null || fileName.startsWith("${")) {
            fileName = DEFAULT_CONFIG_FILE_LOCATION;
        }
        File config = new File(fileName);

        if (!config.isFile() || !config.canRead()) {
            throw new IllegalStateException(String.format("Config file %1$s either does not exist or not readable", config.getAbsoluteFile()));
        }
        watchTimer.scheduleAtFixedRate(new JmxConfigurer(jmxDone, config), 0l, TimeUnit.SECONDS.toMillis(WATCH_HOW_OFTEN));
        DefaultExports.initialize();

        try {
            jmxDone.await(10, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            throw new RuntimeException("Failed waiting for JMX registry", e);
        }

        FileInfo info = lastChange.get();
        if (info.exception != null) {
            throw new IllegalStateException(String.format("Failed to initialise agent from file %1$s", config.getAbsolutePath()), info.exception);
        }

        isInitialised = true;
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }

    private static void chekInitialised() {
        if (!isInitialised) {
            throw new IllegalStateException("Context registry is not initialised!");
        }
    }

    /**
     * Returns status of config file
     *
     * @return
     */
    public static FileInfo latestStatus() {
        return lastChange.get();
    }

    /**
     * Obtains collector registry
     *
     * @return
     * @throws IllegalStateException if registry not initialised
     */
    public static CollectorRegistry getRegistry() {
        chekInitialised();

        return CollectorRegistry.defaultRegistry;
    }
}
