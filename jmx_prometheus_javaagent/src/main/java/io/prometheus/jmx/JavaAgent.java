package io.prometheus.jmx;

import io.prometheus.client.Collector;
import io.prometheus.client.CollectorRegistry;
import io.prometheus.client.exporter.common.TextFormat;
import io.prometheus.client.hotspot.DefaultExports;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.thread.QueuedThreadPool;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Writer;
import java.lang.instrument.Instrumentation;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

public class JavaAgent {
    private final static Timer watchTimer = new Timer("Prometheus config watch", true);
    private final static AtomicReference<FileInfo> lastChange = new AtomicReference<FileInfo>();
    static Server server;

    public static void premain(String agentArgument, Instrumentation instrumentation) throws Exception {
        final String[] args = agentArgument.split(":");
        if (args.length < 2) {
            System.err.println("Usage: -javaagent:/path/to/JavaAgent.jar=<port>:<yaml configuration file>[:<config watch timeout - default 30s>]");
            System.exit(1);
        }

        long timeout = 30l;

        if (args.length > 2) {
            timeout = Long.parseLong(args[2]);
        }

        final CountDownLatch jmxDone = new CountDownLatch(1);
        watchTimer.scheduleAtFixedRate(new JmxConfigurer(jmxDone, new File(args[1])), 0l, TimeUnit.SECONDS.toMillis(timeout));
        DefaultExports.initialize();

        try {
            jmxDone.await(10, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            throw new RuntimeException("Failed waiting for JMX registry", e);
        }

        FileInfo info = lastChange.get();
        if (info.exception != null) {
            throw new IllegalStateException("Failed to initialise agent", info.exception);
        }

        int port = Integer.parseInt(args[0]);
        server = new Server(port);
        QueuedThreadPool pool = new QueuedThreadPool(4);
        pool.setMaxQueued(1);
        pool.setDaemon(true);
        server.setThreadPool(pool);
        for (Connector conn : server.getConnectors()) {
            conn.setMaxIdleTime((int)TimeUnit.MILLISECONDS.convert(20, TimeUnit.SECONDS));
        }
        ServletContextHandler context = new ServletContextHandler();
        context.setContextPath("/");

        server.setHandler(context);
        context.addServlet(new ServletHolder(new BetterMetricsServlet()), "/metrics");
        server.start();
    }


    public static class BetterMetricsServlet extends HttpServlet {

        private CollectorRegistry registry;

        /**
         * Construct a MetricsServlet for the default registry.
         */
        public BetterMetricsServlet() {
            this(CollectorRegistry.defaultRegistry);
        }

        /**
         * Construct a MetricsServlet for the given registry.
         */
        public BetterMetricsServlet(CollectorRegistry registry) {
            this.registry = registry;
        }

        @Override
        //because metrics servlet was not closing writer correctly
        protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
                throws ServletException, IOException {

            //First we need to eagerly resolve shit, then write it out.
            List<Collector.MetricFamilySamples> eager = Collections.list(registry.metricFamilySamples());

            resp.setStatus(HttpServletResponse.SC_OK);
            resp.setContentType(TextFormat.CONTENT_TYPE_004);

            try (Writer writer = resp.getWriter()) {
                TextFormat.write004(writer, Collections.enumeration(eager));
            }
        }

        @Override
        protected void doPost(final HttpServletRequest req, final HttpServletResponse resp)
                throws ServletException, IOException {
            doGet(req, resp);
        }

    }

    static class FileInfo {
        final long lastChangeTs;
        final String fileName;
        final Throwable exception;

        FileInfo(long lastChangeTs, String fileName) {
            this(lastChangeTs, fileName, null);
        }

        public FileInfo(long lastChangeTs, Throwable exception) {
            this(lastChangeTs, null, exception);
        }

        public FileInfo(long lastChangeTs, String fileName, Throwable exception) {
            this.lastChangeTs = lastChangeTs;
            this.fileName = fileName;
            this.exception = exception;
        }

    }

    static class JmxConfigurer extends TimerTask {
        final CountDownLatch startLatch;
        final File fileToWatch;
        AtomicReference<Collector> collectorRef = new AtomicReference<>();

        JmxConfigurer(CountDownLatch startLatch, File fileToWatch) {
            this.startLatch = startLatch;
            this.fileToWatch = fileToWatch;
        }

        @Override
        public void run() {
            FileReader reader = null;
            FileInfo info = null;
            try {
                FileInfo lastLoad = lastChange.get();
                long lastModified = fileToWatch.lastModified();
                if (lastLoad != null && lastLoad.lastChangeTs == lastModified) {
                    return;
                }

                //NB: logic below can result in duplicated metric blip for short time
                reader = new FileReader(fileToWatch);
                JmxCollector collector = new JmxCollector(reader);
                Collector old = collectorRef.get();
                if (old != null) {
                    CollectorRegistry.defaultRegistry.unregister(old);
                }
                collectorRef.set(collector.register());

                //NB: here we speculate that lastModified used for loading collector is same as file API returned previously
                info = new FileInfo(lastModified, fileToWatch.getCanonicalPath());
            } catch (Throwable e) {
                info = new FileInfo(new Date().getTime(), e);
            } finally {
                lastChange.set(info);
                startLatch.countDown();
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    //ignore
                }
            }
        }
    }

}
