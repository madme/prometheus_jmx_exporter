package io.prometheus.jmx;

import io.prometheus.client.Collector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.String.format;

/**
 * Extracted from being inner class
 */
public class JmxMetricReceiver implements JmxScraper.MBeanReceiver {

    private Logger LOGGER = LoggerFactory.getLogger(getClass());

    private final JmxCollector.Config config;

    private static final char SEP = '_';
    private final Pattern unsafeChars = Pattern.compile("[^a-zA-Z0-9:_]");
    private final Pattern multipleUnderscores = Pattern.compile("__+");
    Map<String, Collector.MetricFamilySamples> metricFamilySamplesMap =
            new HashMap<>();

    private static final Pattern snakeCasePattern = Pattern.compile("([a-z0-9])([A-Z])");

    public JmxMetricReceiver(JmxCollector.Config config) {
        this.config = config;
    }

    // [] and () are special in regexes, so swtich to <>.
    private String angleBrackets(String s) {
        return "<" + s.substring(1, s.length() - 1) + ">";
    }

    private String safeName(String s) {
        // Change invalid chars to underscore, and merge underscores.
        return multipleUnderscores.matcher(unsafeChars.matcher(s).replaceAll("_")).replaceAll("_");
    }

    public void addSample(Collector.MetricFamilySamples.Sample sample, Collector.Type type, String help) {
        Collector.MetricFamilySamples mfs = metricFamilySamplesMap.get(sample.name);
        if (mfs == null) {
            // JmxScraper.MBeanReceiver is only called from one thread,
            // so there's no race here.
            mfs = new Collector.MetricFamilySamples(sample.name, type, help, new ArrayList<Collector.MetricFamilySamples.Sample>());
            metricFamilySamplesMap.put(sample.name, mfs);
        }
        mfs.samples.add(sample);
    }

    private void defaultExport(
            String domain,
            LinkedHashMap<String, String> beanProperties,
            LinkedList<String> attrKeys,
            String attrName,
            String attrType,
            String help,
            Object value) {
        StringBuilder name = new StringBuilder();
        name.append(domain);
        if (beanProperties.size() > 0) {
            name.append(SEP);
            name.append(beanProperties.values().iterator().next());
        }
        for (String k : attrKeys) {
            name.append(SEP);
            name.append(k);
        }
        name.append(SEP);
        name.append(attrName);
        String fullname = safeName(name.toString());

        if (config.lowercaseOutputName) {
            fullname = fullname.toLowerCase();
        }

        List<String> labelNames = new ArrayList<>();
        List<String> labelValues = new ArrayList<>();
        if (beanProperties.size() > 1) {
            Iterator<Map.Entry<String, String>> iter = beanProperties.entrySet().iterator();
            // Skip the first one, it's been used in the name.
            iter.next();
            while (iter.hasNext()) {
                Map.Entry<String, String> entry = iter.next();
                String labelName = safeName(entry.getKey());
                if (config.lowercaseOutputLabelNames) {
                    labelName = labelName.toLowerCase();
                }
                labelNames.add(labelName);
                labelValues.add(entry.getValue());
            }
        }

        addSample(new Collector.MetricFamilySamples.Sample(fullname, labelNames, labelValues, ((Number) value).doubleValue()),
                Collector.Type.GAUGE, help);
    }

    public void recordBean(
            String domain,
            LinkedHashMap<String, String> beanProperties,
            LinkedList<String> attrKeys,
            String attrName,
            String attrType,
            String attrDescription,
            Object beanValue) {

        Number value;
        if (beanValue instanceof Number) {
            value = (Number) beanValue;
        } else if (beanValue instanceof Boolean) {
            value = (Boolean) beanValue ? 1 : 0;
        } else {
            LOGGER.debug("Ignoring non-Number/Boolean bean: {}{}{}{} : {} ", domain, beanProperties, attrKeys, attrName, beanValue);
            return;
        }

        String beanName = domain +
                angleBrackets(beanProperties.toString()) +
                angleBrackets(attrKeys.toString());
        // attrDescription tends not to be useful, so give the fully qualified name too.
        String help = attrDescription + " (" + beanName + attrName + ")";

        String attrNameSnakeCase = snakeCasePattern.matcher(attrName).replaceAll("$1_$2").toLowerCase();

        for (JmxCollector.Rule rule : config.rules) {
            Matcher matcher = null;
            String matchName = beanName + (rule.attrNameSnakeCase ? attrNameSnakeCase : attrName);
            if (rule.pattern != null) {
                matcher = rule.pattern.matcher(matchName + ": " + value);
                if (!matcher.matches()) {
                    continue;
                }
            }
            // If there's no name provided, use default export format.
            if (rule.name == null) {
                defaultExport(domain, beanProperties, attrKeys, rule.attrNameSnakeCase ? attrNameSnakeCase : attrName, attrType, help, value);
                return;
            }
            // Matcher is set below here due to validation in the constructor.
            String name = safeName(matcher.replaceAll(rule.name));
            if (name.isEmpty()) {
                return;
            }
            if (config.lowercaseOutputName) {
                name = name.toLowerCase();
            }
            // Set the help.
            if (rule.help != null) {
                help = matcher.replaceAll(rule.help);
            }
            // Set the labels.
            List<String> labelNames = new ArrayList<>();
            List<String> labelValues = new ArrayList<>();
            if (rule.labels != null) {
                for (Map.Entry<String, String> l : rule.labels.entrySet()) {
                    final String unsafeLabelName = l.getKey();
                    final String labelValReplacement = l.getValue();
                    try {
                        String labelName = safeName(matcher.replaceAll(unsafeLabelName));
                        String labelValue = matcher.replaceAll(labelValReplacement);
                        if (config.lowercaseOutputLabelNames) {
                            labelName = labelName.toLowerCase();
                        }
                        if (!labelName.isEmpty() && !labelValue.isEmpty()) {
                            labelNames.add(labelName);
                            labelValues.add(labelValue);
                        }
                    } catch (Exception e) {
                        throw new RuntimeException(
                                format("Matcher '%s' unable to use: '%s' value: '%s'", matcher, unsafeLabelName, labelValReplacement), e);
                    }
                }
            }
            // Add to samples.
            LOGGER.debug("add metric sample: {} {} {} {}", name, labelNames, labelValues, value.doubleValue());
            addSample(new Collector.MetricFamilySamples.Sample(name, labelNames, labelValues, value.doubleValue()), rule.type, help);
            return;
        }
    }

}