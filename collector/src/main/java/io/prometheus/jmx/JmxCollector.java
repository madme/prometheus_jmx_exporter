package io.prometheus.jmx;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import io.prometheus.client.Collector;
import io.prometheus.client.CollectorRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import java.io.IOException;
import java.io.Reader;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.regex.Pattern;

public class JmxCollector extends Collector {
    private static final Logger LOGGER = LoggerFactory.getLogger(JmxCollector.class.getName());


    private static final List<Rule> DEFAULT_RULE = Collections.singletonList(new Rule());

    //pool size is 2 so we can collapse tasks that were scheduled concurrently
    private static final int POOL_SIZE_TO_COLLAPSE_OVERLAPPING_TASKS = 2;

    private final Config config;

    @JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
    public static class Config {
        public String jmxUrl = "";
        public String username = "";
        public String password = "";
        public String hostPort;

        public boolean lowercaseOutputName;
        public boolean lowercaseOutputLabelNames;
        public List<ObjectName> whitelistObjectNames = new ArrayList<>();
        public List<ObjectName> blacklistObjectNames = new ArrayList<>();
        public List<Pattern> blacklistPatterns = new ArrayList<>();
        public List<Rule> rules = DEFAULT_RULE;

    }

    @JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
    public static class Rule {
        @JsonIgnore
        public Pattern pattern;
        @JsonProperty(required = true)
        public String name;
        public String help;
        public boolean attrNameSnakeCase;
        public Type type = Type.GAUGE;
        public Map<String, String> labels;

        @JsonSetter
        public void setPattern(String pattern) {
            this.pattern = Pattern.compile("^.*" + pattern + ".*$");
        }
    }


    public JmxCollector(Reader in) throws IOException, MalformedObjectNameException {
        this(new ObjectMapper(new YAMLFactory()).readValue(in, Config.class));
    }

    public JmxCollector(String yamlConfig) throws MalformedObjectNameException, IOException {
        this(new ObjectMapper(new YAMLFactory()).readValue(yamlConfig, Config.class));
    }

    public JmxCollector(Config config) throws MalformedObjectNameException {
        if (config == null) {  //Yaml config empty, set config to empty map.
            config = new Config();
        }

        if (config.hostPort != null) {
            if (config.jmxUrl != null) {
                throw new IllegalArgumentException("At most one of hostPort and jmxUrl must be provided");
            }
            config.jmxUrl = "service:jmx:rmi:///jndi/rmi://" + config.hostPort + "/jmxrmi";
        } else if (config.jmxUrl == null) {
            // Default to local JVM
            config.jmxUrl = "";
        }

        if (config.whitelistObjectNames.size() == 0) {
            config.whitelistObjectNames.add(null);
        }

        if (config.rules != DEFAULT_RULE) {
            for (Rule rule : config.rules) {
                // Validation.
                if ((rule.labels != null || rule.help != null) && rule.name == null) {
                    throw new IllegalArgumentException("Must provide name, if help or labels are given: " + rule.name);
                }
                if (rule.name != null && rule.pattern == null) {
                    throw new IllegalArgumentException("Must provide pattern, if name is given: " + rule.name);
                }

            }
        }

        this.config = config;
    }

    @Override
    //We'll force collection of first metrics on register
    public <T extends Collector> T register(CollectorRegistry registry) {
        //first collect metrics then register
        collect(true);
        return super.register(registry);
    }

    private AtomicReference<List<MetricFamilySamples>> results = new AtomicReference<>(Collections.<MetricFamilySamples>emptyList());

    private ReadWriteLock rwLock = new ReentrantReadWriteLock();

    private ExecutorService sv = Executors.newFixedThreadPool(POOL_SIZE_TO_COLLAPSE_OVERLAPPING_TASKS);

    /**
     * Allows to clean certain resources
     */
    public void stop() {
        sv.shutdownNow();
    }

    private List<MetricFamilySamples> collect(boolean force) {
        Callable<List<MetricFamilySamples>> c = new Callable<List<MetricFamilySamples>>() {
            @Override
            public List<MetricFamilySamples> call() throws Exception {
                Lock l = rwLock.writeLock();
                if (!l.tryLock()) {
                    //if we can't get lock - do nothing
                    return results.get();
                }

                try {
                    //todo - this stuff should be optimised
                    JmxMetricReceiver receiver = new JmxMetricReceiver(config);
                    JmxScraper scraper = new JmxScraper(config.jmxUrl, config.username, config.password, config.whitelistObjectNames, config.blacklistObjectNames, config
                            .blacklistPatterns, receiver);
                    long start = System.nanoTime();
                    long scrapeBeginTime = new Date().getTime();
                    double error = 0;
                    try {
                        scraper.doScrape();
                    } catch (Exception e) {
                        error = 1;
                        LOGGER.warn("JMX scrape failed: ", e);
                    }

                    receiver.addSample(new MetricFamilySamples.Sample(
                            "jmx_scrape_duration_seconds", Collections.<String>emptyList(), Collections.<String>emptyList(), (System.nanoTime() - start) / 1.0E9), Type.GAUGE, "Time this JMX " +
                            "scrape took, in" +
                            " seconds.");
                    receiver.addSample(new MetricFamilySamples.Sample(
                            "jmx_scrape_start_time_ms", Collections.<String>emptyList(), Collections.<String>emptyList(), (double) scrapeBeginTime), Type.GAUGE, "Time this JMX scrape started, " +
                            "unix time, ms.");
                    receiver.addSample(new MetricFamilySamples.Sample(
                            "jmx_scrape_error", Collections.<String>emptyList(), Collections.<String>emptyList(), error), Type.GAUGE, "Non-zero if this scrape failed.");
                    //have to convert into array list because Collector specifies too narrow interface while it only uses List.iterator !
                    // another big deal is that we lose streaming immediately when we have to gather stats and compact metrics
                    //  also we have to have stable metric family names due to prometheus protocol limitations, thus there is limit to streaming
                    List<MetricFamilySamples> result = new ArrayList<>(receiver.metricFamilySamplesMap.values());
                    results.set(result);
                    return result;
                } finally {
                    l.unlock();
                }
            }
        };

        if (force) {
            try {
                return c.call();
            } catch (Exception e) {
                throw new IllegalStateException(e);
            }
        } else {
            sv.submit(c);
            return results.get();
        }
    }

    @Override
    public List<MetricFamilySamples> collect() {
        return collect(false);
    }
}
